sudo apt-get install exuberant-ctags cmake libevent-dev libncurses5-dev
wget https://github.com/tmux/tmux/releases/download/2.3/tmux-2.3.tar.gz
tar -xvzf tmux-2.3.tar.gz
cd tmux-2.3
./configure && make
sudo make install
cd ~
rm tmux-2.3.tar.gz
mkdir bin
cd bin
git clone https://livando@bitbucket.org/livando/dotfiles.git
cd ~
ln -s ~/bin/dotfiles/vim .vim
ln -s ~/bin/dotfiles/vim/vimrc .vimrc
ln -s ~/bin/dotfiles/tmux.conf .tmux.conf
echo ". ~/bin/dotfiles/bashrc" >> ~/.bashrc
. ~/.bashrc
rm -rf ~/.vim/bundle/*
git clone https://github.com/mileszs/ack.vim.git ~/.vim/bundle/ack.vim
git clone https://github.com/kien/ctrlp.vim.git ~/.vim/bundle/ctrlp.vim
git clone https://github.com/vim-syntastic/syntastic.git ~/.vim/bundle/syntastic.vim
git clone https://github.com/tpope/vim-bundler.git ~/.vim/bundle/vim-bundler.vim
git clone https://github.com/tpope/vim-fugitive.git ~/.vim/bundle/vim-fugitive.vim
git clone https://github.com/tpope/vim-rails.git ~/.vim/bundle/vim-rails.vim
git clone https://github.com/vim-ruby/vim-ruby.git ~/.vim/bundle/vim-ruby.vim
git clone https://github.com/tpope/vim-sensible.git ~/.vim/bundle/vim-sensible.vim
git clone https://github.com/tpope/vim-bundler.git ~/.vim/bundler/vim-bundler.vim
git clone https://github.com/thoughtbot/vim-rspec.git ~/.vim/bundler/vim-rspec.vim
git clone https://github.com/jszakmeister/vim-togglecursor ~/.vim/togglecursor.vim
